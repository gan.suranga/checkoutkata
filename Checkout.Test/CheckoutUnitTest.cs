using System;
using System.Collections.Generic;
using Xunit;

namespace Checkout.Test
{
    public class CheckoutUnitTest
    {
        private ICheckout _checkout;
        public CheckoutUnitTest() {


            List<Item> items = new List<Item>
            {
                new Item {Sku = "A99", UnitPrice=0.50m},
                new Item {Sku = "B15", UnitPrice=0.30m},
                new Item {Sku = "C40", UnitPrice=0.60m}
            };

            IEnumerable<Discount> discounts = new List<Discount>()
            {
                new Discount{Item = "A99", Count = 3, DiscountedValue = -0.20m},
                new Discount{Item = "B15", Count = 2, DiscountedValue = -0.15m}
            };

            _checkout = new Checkout(items, discounts);
        }

        [Fact]
        public void When_No_Item_Is_Scanned_Should_Return_0()
        {
             _checkout.Scan(null);
            Assert.Equal(0.0m,_checkout.Total());
        }

        [Fact]
        public void When_ItemA99_Is_Scanned_Should_Return_Its_Price() {

            var item = new Item { Sku = "A99", UnitPrice = 0.5m };
            _checkout.Scan(item);
            Assert.Equal(0.5m, _checkout.Total());        
        }

        [Fact]
        public void When_ItemB15_Is_Scanned_Should_Return_Its_Price()
        {

            var item = new Item { Sku = "B15", UnitPrice = 0.3m };
            _checkout.Scan(item);
            Assert.Equal(0.3m, _checkout.Total());
        }

        [Fact]
        public void When_ItemC40_Is_Scanned_Should_Return__Its_Price()
        {
            var item = new Item {  Sku = "C40", UnitPrice = 0.6m  };

            _checkout.Scan(item);
            Assert.Equal(0.6m, _checkout.Total());
        }

        [Fact]
        public void When_Multiple_Items_Scanned_Should_Return_Correct_Total()
        {
            var itemC = new Item { Sku = "C40", UnitPrice = 0.6m };
            var itemB = new Item { Sku = "B15", UnitPrice = 0.3m };
            var itemA = new Item { Sku = "A99", UnitPrice = 0.5m };

            _checkout.Scan(itemC);
            _checkout.Scan(itemB);
            _checkout.Scan(itemA);
            _checkout.Scan(itemA);
            Assert.Equal(1.9m, _checkout.Total());
        }

        [Fact]
        public void When_2_ItemB15s_are_Scanned_Should_Return_the_Total_With_Discount_Applied()
        {

            var item = new Item   { Sku = "B15",  UnitPrice = 0.3m };
                        
            _checkout.Scan(item);
            _checkout.Scan(item);
            Assert.Equal(0.45m, _checkout.Total());

        }

        [Fact]
        public void When_2_ItemsA99_are_Scanned_Should_Return_the_Total_With_Discount_Applied()
        {

            var item = new Item
            {
                Sku = "A99",
                UnitPrice = 0.5m
            };

            _checkout.Scan(item);
            _checkout.Scan(item);
            _checkout.Scan(item);
            Assert.Equal(1.30m, _checkout.Total());

        }

        [Fact]
        public void When_3_ItemsA99_And_2_B15are_Scanned_Should_Return_the_Total_With_Discount_Applied()
        {
            var itemB = new Item
            {
                Sku = "B15",
                UnitPrice = 0.3m
            };

            var itemA = new Item
            {
                Sku = "A99",
                UnitPrice = 0.5m
            };

            _checkout.Scan(itemA);
            _checkout.Scan(itemB);
            _checkout.Scan(itemA);
            _checkout.Scan(itemB);
            _checkout.Scan(itemA);
            Assert.Equal(1.75m, _checkout.Total());

        }
    }
}
