﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Checkout
{
    public class Checkout: ICheckout
    {

        private decimal _sum;
        private IList<Item> _scannedItems = new List<Item>();
        private readonly IEnumerable<IDiscount> _discounts;
        private readonly IEnumerable<Item> _items;

        public Checkout(IEnumerable<Item> items, IEnumerable<IDiscount> discounts) {

            _items = items;
            _discounts = discounts;
        }

        public decimal Total() {
            _sum = 0;
          
            foreach(var scannedItem in _scannedItems)
            {
                _sum += scannedItem.UnitPrice;  
            }
            ApplyDiscount(_scannedItems, _discounts);
            return _sum;
        
        }
        public void Scan(Item item) {
            if (item == null)
                return;

            _scannedItems.Add(
                _items.FirstOrDefault(x => x.Sku == item.Sku)
                );

        }

        private void ApplyDiscount(IEnumerable<Item> scannedItems, IEnumerable<IDiscount> discounts)
        {
            var groupedSimiarItems = scannedItems.GroupBy(x => x.Sku).Select(x => new { ItemName = x.Key, Count = x.Count() });


            foreach (var groupedItem in groupedSimiarItems)
            {
                foreach (var discount in discounts)
                {
                    if (groupedItem.ItemName == discount.Item)
                    {
                        var multiples = (groupedItem.Count) / discount.Count;
                        _sum = _sum + (discount.DiscountedValue) * multiples;
                    }
                }

            }
        }
    }

    
}
