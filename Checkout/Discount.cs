﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Checkout
{
    public class Discount :IDiscount
    {
        public string Item { get; set; }
        public int Count { get; set; }
        public decimal DiscountedValue { get; set; }
    }
}
