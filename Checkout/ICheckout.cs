﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Checkout
{
    public interface ICheckout
    {
        void Scan(Item item);
        decimal Total();
    }
}
