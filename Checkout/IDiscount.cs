﻿namespace Checkout
{
    public interface IDiscount
    {
       string Item { get; set; }
       int Count { get; set; }
       decimal DiscountedValue { get; set; }
    }
}