﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Checkout
{
    public class Item
    {
        public string Sku { get; set; }
        public decimal UnitPrice { get; set; }
    }
}
